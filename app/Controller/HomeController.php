<?php
namespace App\Controller;

use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Annotations as M;
use Avris\Micrus\Controller\Http\Response;

class HomeController extends Controller
{

    /**
     * @M\Route("/")
     */
    public function homeAction()
    {
        return $this->render(['clock' => $this->getClock()]);
    }

    /**
     * @M\Route("/futurize", name="futurize")
     */
    public function futurizeAction()
    {
        return new Response($this->getService('futurus')->futurizeText($this->getData('text')));
    }

    /**
     * @M\Route("/web", name="webGenerate")
     */
    public function webGenerateAction()
    {
        return ($url = $this->getQuery('url')) ?
            $this->redirectToRoute('web', ['url' => $url]) :
            $this->redirectToRoute('home');
    }

    /**
     * @M\Route("/web/{url}", name="web", unrestricted=true)
     */
    public function webAction($url)
    {
        $get = $this->getQuery()->getArray();
        unset($get['_controller']);
        $url = rtrim($url . '?' . http_build_query($get), '?');

        try {
            return $this->getService('futurus.browser')->futurizePage($url);
        } catch (\Exception $e) {
            return $this->render([
                '_view' => 'Error/curl.html.twig',
                'error' => $e->getMessage(),
                'url' => $url,
            ]);
        }
    }

    /**
     * @M\Route("/clock/{hours}/{minutes}/{format}", name="clock", defaults={"format": 2})
     */
    public function clockAction($hours, $minutes, $format = 2)
    {
        $time = new \DateTime($hours . ':' . $minutes);
        return $this->renderJson([
            'text' => $this->getClock($time, $format),
            'time' => $time->format('Y-m-d H:i:s'),
        ]);
    }

    protected function getClock($time = 'now', $format = 2)
    {
        return $this->getService('futurus')->futurizeText(
            $this->getService('stringer')->timeInWords($time, $format)
        );
    }
}