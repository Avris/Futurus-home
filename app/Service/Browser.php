<?php
namespace App\Service;

use App\Model\Response;
use Avris\Futurus\Futurus;
use Avris\Micrus\Controller\Routing\Router;

class Browser
{
    /** @var \DOMDocument */
    protected $dom;

    /** @var string */
    protected $url;

    /** @var Futurus */
    protected $futurus;

    /** @var Router */
    protected $router;

    /** @var string */
    protected $cookieFile;

    /**
     * @param Futurus $futurus
     * @param Router $router
     * @param string $rootDir
     */
    public function __construct(Futurus $futurus, Router $router, $rootDir)
    {
        $this->futurus = $futurus;
        $this->router = $router;
        $cookieDir = $rootDir . '/run/cache/cookies/';
        if (!is_dir($cookieDir)) { mkdir($cookieDir); }
        $this->cookieFile = $cookieDir . session_id();
    }

    /**
     * @param string $url
     * @return Response
     * @throws \Exception
     */
    public function futurizePage($url)
    {
        $this->url = preg_match('#^https?://#ui', $url) ? $url : 'http://' . $url;

        $response = $this->curl($this->url, $_SERVER['REQUEST_METHOD'], $_POST);

        return $response->setContent(
            $response->getContentType() == 'text/html' ?
                $this->futurizeHtml($response->getContent()) :
                $this->futurus->futurizeText($response->getContent())
        );
    }

    /**
     * @param $url
     * @param $method
     * @param $data
     * @return Response
     * @throws \Exception
     */
    protected function curl($url, $method, $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        switch ($method) {
            case 'GET': break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case 'PUT':
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case 'HEAD':
                break;
            default:
                throw new \Exception('Invalid request method');
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }

        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);

        $response = curl_exec($ch);
        if ($response === false || curl_errno($ch)) { throw new \Exception(curl_error($ch)); }
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return new Response(
            $this,
            substr($response, $headerSize),
            $httpCode,
            substr($response, 0, $headerSize)
        );
    }

    /**
     * @param string $body
     * @return mixed
     */
    protected function futurizeHtml($body)
    {
        $this->dom = new \DOMDocument();
        if (@$this->dom->loadHtml($body, LIBXML_HTML_NODEFDTD)) {
            $this->handleNode($this->dom);
            return $this->dom->saveHTML();
        }

        return $this->futurus->futurizeText($body);
    }

    /**
     * @param \DOMNode $node
     */
    protected function handleNode(\DOMNode $node)
    {
        if ($node instanceof \DOMCdataSection) { return; }

        if ($node instanceof \DOMText) {
            if (!trim($node->wholeText)) { return; }
            $replaced = $this->futurus->futurizeText($node->wholeText);
            $node->replaceData(0, strlen($node->wholeText), $replaced);
        }

        if ($node instanceof \DOMElement) {
            if ($node->tagName == 'meta' && $node->hasAttribute('charset')) {
                $node->setAttribute('charset', 'utf-8');
            }

            $this->nodeFuturizeAttribute($node, 'title');
            $this->nodeFuturizeAttribute($node, 'alt');

            $this->nodeUrlToAbsolute($node, 'src');
            $this->nodeUrlToAbsolute($node, 'data-src');
            $this->nodeUrlToAbsolute($node, 'href');
            $this->nodeUrlToAbsolute($node, 'action');

            $this->nodeUrlPrependBrowser($node, 'a', 'href');
            $this->nodeUrlPrependBrowser($node, 'form', 'action');
        }

        if (count($node->childNodes)) {
            /** @var \DOMNode $childNode */
            foreach ($node->childNodes as $childNode) {
                $this->handleNode($childNode);
            }
        }
    }

    /**
     * Based on http://nashruddin.com/PHP_Script_for_Converting_Relative_to_Absolute_URL
     *
     * @param string $relative
     * @param string $base
     * @return string
     */
    protected function relativeToAbsolute($relative, $base)
    {
        if (!$relative) { return $base; }

        if (substr($relative, 0, 2) == '//') { return $relative; }

        if (parse_url($relative, PHP_URL_SCHEME) != '') {
            return $relative;
        }

        if (in_array($relative[0], ['#','?'])) {
            return $base . $relative;
        }

        $baseParts = parse_url($base);

        $path = preg_replace('#/[^/]*$#', '', isset($baseParts['path']) ? $baseParts['path'] : '');

        if ($relative[0] == '/') { $path = ''; }

        $absolute = $baseParts['host'] . $path . '/' . $relative;

        /* replace '//' or '/./' or '/foo/../' with '/' */
        $re = ['#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#'];
        for ($n = 1; $n > 0; $absolute = preg_replace($re, '/', $absolute, -1, $n));

        return $baseParts['scheme'] . '://' . $absolute;
    }

    /**
     * @param \DOMElement $node
     */
    protected function nodeFuturizeAttribute(\DOMElement $node, $attr)
    {
        if ($node->hasAttribute($attr)) {
            $node->setAttribute($attr, $this->futurus->futurizeText($node->getAttribute($attr)));
        }
    }

    /**
     * @param \DOMElement $node
     */
    protected function nodeUrlToAbsolute(\DOMElement $node, $attr)
    {
        if ($node->hasAttribute($attr)) {
            $node->setAttribute($attr, $this->relativeToAbsolute($node->getAttribute($attr), $this->url));
        }
    }

    /**
     * @param \DOMElement $node
     */
    protected function nodeUrlPrependBrowser(\DOMElement $node, $tagName, $attr)
    {
        if ($node->hasAttribute($attr) && $node->tagName == $tagName) {
            $node->setAttribute($attr, $this->urlPrependBrowser($node->getAttribute($attr)));
        }
    }

    /**
     * @param string $url
     * @return string
     */
    public function urlToAbsoluteAndWithBrowser($url)
    {
        return $this->urlPrependBrowser($this->relativeToAbsolute($url, $this->url));
    }

    /**
     * @param string $url
     * @return string
     */
    protected function urlPrependBrowser($url)
    {
        return $this->router->getUrl('web', ['url' => $url]) . (substr($url, -1) == '/' ? '/' : '');
    }
}
