<?php
namespace App\Model;

use App\Service\Browser;
use Avris\Micrus\ParameterBag;

class Response extends \Avris\Micrus\Controller\Http\Response
{
    /** @var Browser */
    protected $browser;

    /** @var string */
    protected $contentType;

    /** @var string */
    protected $charset;

    /**
     * @param Browser $browser
     * @param int $content
     * @param int $status
     * @param array $headers
     */
    public function __construct(Browser $browser, $content, $status = 200, $headers = array())
    {
        $this->browser = $browser;
        $this->headers = new ParameterBag($this->parseHeaders($headers));
        $this->status = (int) $status;
        $this->content = $this->utf8encode($content);
    }

    /**
     * @param string $headers
     * @return array
     */
    protected function parseHeaders($headers)
    {
        $output = [];
        foreach (explode("\r\n", $headers) as $header) {
            if (!trim($header)) { continue; }
            if (strpos($header, ': ') === false) { continue; }
            $data = explode(': ', $header);
            $data[0] = strtolower($data[0]);
            if ($data[0] == 'location') {
                $data[1] = $this->browser->urlToAbsoluteAndWithBrowser($data[1]);
            } else if ($data[0] == 'transfer-encoding') {
                continue;
            } else if ($data[0] == 'set-cookie') {
                continue;
            } else if ($data[0] == 'content-type') {
                $ct = explode('; charset=', $data[1]);
                $this->contentType = $ct[0];
                $this->charset = isset($ct[1]) ? $ct[1] : null;
            }
            $output[$data[0]] = $data[1];
        }

        return $output;
    }

    /**
     * @param $text
     * @return string
     */
    protected function utf8encode($text)
    {
        mb_convert_encoding(
            $text,
            $this->contentType == 'text/html' ? 'HTML-ENTITIES' : 'UTF-8',
            $this->charset ?: 'UTF-8'
        );

        if ($this->contentType) {
            $this->headers->set('content-type', $this->contentType . '; charset=UTF-8');
        }

        $this->charset = 'UTF-8';

        return $text;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

}
