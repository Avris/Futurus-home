$(document).ready(function() {
    const body = $('html, body');
    const input = $('#input');
    const generate = $('#generate');
    const randomText = $('#random-text');
    const output = $('#output');
    const manifestImg = $('#manifest-scan-img');
    const manifestSpinner = $('#manifest-scan-spinner');
    const generatorSpinner = $('#generator-spinner');

    let currentStart = '';
    let scrolled = false;

    const scroll = function() {
        if (scrolled) { return false; }
        const list = [];
        $('article').each(function() {
            if ($(document).scrollTop() >= ($(this).position().top - 30)) { list.push($(this).attr('id')); }
        });
        const id = list.length === 0 ? '' : list.slice(-1)[0];
        if (id !== currentStart) {
            currentStart = id;
            if (history.pushState) {
                history.pushState(null, null, `#${id}`);
            } else {
                location.hash = id;
            }
        }
    };
    scroll();
    $(document).scroll(scroll);

    $('a').click(function() {
        const href = $(this).attr('href').match(/#[A-Za-z0-9-_]*/);
        if (!href || $(this).data('toggle') || (href[0].substr(0,1) !== '#')) { return; }

        const top = href[0] === '#' ? 0 : $(href[0]).position().top;
        scrolled = true;
        body.animate({scrollTop: top}, 400, function() { scrolled = false; scroll(); });
        return false;
    });

    randomText.click(function() {
        generatorSpinner.show();
        $.get(
            'https://stoslowia.avris.it/.json',
            function(data) {
                const article = data[Math.floor(Math.random() * data.length)];
                input.html(article['title'] + "\r\n\r\n" + article['content'] + "\r\n\r\n" + "http://stoslowia.avris.it/s/" + article['id']);
                return generate.click();
            },
            'json'
        );
    });

    generate.click(function() {
        generatorSpinner.show();
        $.post(
            M.route('futurize'),
            { text: input.val() },
            function(data) {
                output.val(data);
                generatorSpinner.hide();
            });
    });

    output.keydown(function(e) { if (!e.ctrlKey) { return false; } });

    $('a[href=#manifest-scan]').click(function() {
        if (!manifestImg.attr('src')) {
            manifestImg.attr('src', M.asset('assetic/gfx/Manifest.jpg')).load(() => manifestSpinner.remove());
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    const clock = $('#clock');
    if (clock.length) {
        const getTime = function() {
            const d = new Date();
            return [d.getHours(), d.getMinutes()];
        };
        let currentTime = [-1,-1];
        const updateClock = function() {
            const newTime = getTime();
            if (currentTime[1] === newTime[1]) { return; }
            currentTime = newTime;
            $.get(
                M.route('clock', {hours: currentTime[0], minutes: currentTime[1]}),
                [],
                data => clock.html(data.text));
        };
        updateClock();

        setInterval(updateClock, 200);
    }
});