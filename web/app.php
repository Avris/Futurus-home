<?php

require_once __DIR__ . '/../vendor/autoload.php';
$app = new Avris\Micrus\App('prod');
$response = $app->run();
$response->send();
